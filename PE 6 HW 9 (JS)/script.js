let firstArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let secondArr = ["1", "2", "3", "sea", "user", 23];
let vstavka = document.getElementById('divf')

function func(array,parent = document.body) {
    let list = document.createElement("ul");
    let liObvertka = array.map((item) => {
        return `<li>${item}</li>`;
    });
    liObvertka = liObvertka.join('');
    list.innerHTML = liObvertka;
      parent.prepend(list)
} 
func(firstArr);
func(secondArr,vstavka);