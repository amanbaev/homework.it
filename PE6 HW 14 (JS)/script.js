let btn = document.querySelector('.btn-changer-theme')
btn.addEventListener('focus',()=>{
  btn.style.backgroundColor = 'Green'
})
btn.addEventListener('blur',()=>{
  btn.style.backgroundColor = 'rgb(255, 0, 0)'
})

let container = document.querySelector('.container')
let textUnderPhoto = document.querySelectorAll('.photo-decription')
let leftMenu = document.querySelectorAll('.left-menu-link')
let mainMenu = document.querySelectorAll('.main-menu-link')
let footerMenu = document.querySelectorAll('.footer-menu-link')
let borderLogo = document.getElementById('borderPhotoLogo')


btn.addEventListener('click',()=>{
borderLogo.classList.remove('border-logo')
borderLogo.classList.add('border-logo-dark')
container.classList.remove('container')
container.classList.add('container-dark')

leftMenu.forEach(el => {
  el.classList.add('darkTheme-text')
})

textUnderPhoto.forEach(el => {
  el.classList.add('darkTheme-text')
})

mainMenu.forEach(el => {
  el.classList.add('darkTheme-text')
})

footerMenu.forEach(el => {
  el.classList.add('darkTheme-text')
})
})

 

