function createNewUser () {
  const newUser = {
    _firstName: " ",
    _lastName: " ",
    birthday: " ",
    age: " ",

    getLogin() {
      return (this._firstName[0] + this._lastName).toLowerCase();
    },
    setFirstName (value) {
      return this._firstName = value;
    },

    setLastName (value) {
      return this._lastName = value;
    },

    getFirstName () {
      return this._firstName;
    },
    getLastName () {
      return this._lastName;
    },
    getAge: function () {
      const birthdayDate = new Date(this.birthday.substr(3, 3) + this.birthday.substr(0, 3) + this.birthday.substr(-4, 4));
      return this.age = new Date(new Date() - birthdayDate).getFullYear() - 1970;
    },
    getPassword () {
      return this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + this.birthday.substr(-4, 4);
    }
  };
  


  newUser.setFirstName(prompt("Your name?"));
  newUser.setLastName(prompt ("Your last name?"));
  newUser.birthday = prompt("Введите дату рожденя", "dd.MM.YYYY");
  return newUser;
};

let user1 = createNewUser();

console.log(user1);
console.log(user1.getPassword());
console.log(user1.getAge());